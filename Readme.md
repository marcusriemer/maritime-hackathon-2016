# Useful URLS

* Ports in the Caribean: ` http://localhost:8080/api/ports?ullat=25.6415263730658&ullon=-50.2734375&lrlat=9.27562217679211&lrlon=-91.4501953125`
# Welcome to some kind of risky port game

# Messages

These messages may be sent over the websocket interface.

## Datatypes

These datatypes are part of multiple messages.

### A geo location

A simple lat/lon location, used for positions of ports, dice stacks, parts of routes ...

    {
        "Lat" : <Latitude, float>
        "Lon" : <Longitude, float>
    }

### A port

A single port at any location, is owned by a player.

    {
        "Name": <Unique name of the Port, string>,
        "Size": <Subjective size of the port, "Big" | "Small">,
        "Location": <Here is the port located, GeoPoint>,
        "OwnerId": <The name of the player this port belongs to, String>,
    }
    
### A group of dice that may be in a port

Dice stacks can be homed in a port or be shipped around.

    {
        "DiceStackId": <Unique ID for a dice stack, int>
        "Location": <Location on map, GeoLocation>
        "CurrentPortId": <Set if this stack is inside a port, string?>
        "OwnerId": <This person owns the dice stack, string>
        "Count": <Number of 6-sided dice, int>
        "Speed": <The travel speed of this stack, float>
        "TargetPortId": <This is where the stack wants to be (maybe nowhere), string?>
    }
    
### A participant

    {
        "Name" : <Unique Name of the player, also used as ID, string>,
        "Color": <A CSS color property, string>
    }

### A whole game

    {
        "Id" : <GameId, int>,
        "State: <State of the Game, "created" | "started" | "simulating" | "ended">
        "Ports": <All Ports, [Port]>,
        "Participants: <All Participants, [Participant]>,
        "DiceStacks": <All Dice Stacks, [DiceStack]>
    }

## Server to Client

These messages are sent from the server to the clients.

### Whole Gamestate

The server will periodically send this to all clients.

    {
        "Type": "all",
        "Payload": <The whole game, Game>
    }

## Client to Server

### Joining an existing game

A player wants to join an existing game.

    {
        "Type": "join",
        "GameId": <GameId, int>
        "Payload:, <Username, string>
    }

### Creating a new game

A player wants to create a new game.

    {
        "Type": "create",
        "Payload" : {
            "NumPorts" : <int>,
            "NumCredit" : <int>,
            "FenceUpperLeft" : <GeoPoint>,
            "FenceLowerRight" : <GeoPoint>,
        }
    }

### Starting a game

A created game should start.

    {
        "Type": "start",
        "GameId": <GameId, int>
    }
