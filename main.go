package main

import (
	"encoding/json"
	"flag"
	"fmt"
	"github.com/gorilla/websocket"
	"log"
	"net/http"
	"strconv"
)

var upgrader = &websocket.Upgrader{ReadBufferSize: 1024, WriteBufferSize: 1024}

type wsHandler struct {
	p *pool
}

func (wsh wsHandler) ServeHTTP(w http.ResponseWriter, r *http.Request) {
	// Promote the connection to a websocket connection
	ws, err := upgrader.Upgrade(w, r, nil)
	if err != nil {
		return
	}

	// Register that connection
	c := &connection{send: make(chan string, 32), ws: ws, p: wsh.p}
	c.p.register <- c

	// Start readers and writers
	go c.reader()
	go c.writer()
}

var (
	addr   = flag.String("addr", "0.0.0.0:8080", "http service address")
	assets = flag.String("assets", "assets", "path to assets")
)

func handlePortData(w http.ResponseWriter, r *http.Request) {
	params := r.URL.Query()
	ullat, err := strconv.ParseFloat(params.Get("ullat"), 64)
	ullon, err := strconv.ParseFloat(params.Get("ullon"), 64)
	lrlat, err := strconv.ParseFloat(params.Get("lrlat"), 64)
	lrlon, err := strconv.ParseFloat(params.Get("lrlon"), 64)

	log.Printf("New Port request:", r.URL)

	if err == nil {
		ul := GeoPoint{Lat: ullat, Lon: ullon}
		lr := GeoPoint{Lat: lrlat, Lon: lrlon}

		ports := GetPortData(ul, lr)
		portsString, err := json.Marshal(ports)

		if err != nil {
			fmt.Fprintf(w, "Error: %s", err)
		}

		w.Write(portsString)
	} else {
		fmt.Fprintf(w, "Error: %s", err)
	}
}

func main() {
	log.Println("Initialising server ...")

	flag.Parse()

	log.Println("Listening on", *addr)

	// GetPortData(GeoPoint{Lat: 25.6415263730658, Lon: -50.2734375},
	// GeoPoint{Lat: 9.27562217679211, Lon: -91.4501953125})

	// Build the connection pool which manages all websocket subscriptions
	p := newPool()
	go p.run()

	go GlobalGameLoop()

	http.Handle("/ws", wsHandler{p: p})
	http.HandleFunc("/api/ports", handlePortData)
	http.Handle("/", http.FileServer(http.Dir(*assets)))
	if err := http.ListenAndServe(*addr, nil); err != nil {
		log.Fatal("ListenAndServe:", err)
	}
}
