PROGNAME = mri-maritime-hackaton-srv
GO_SRCS  = $(wildcard *.go)

all : $(PROGNAME)

run : $(PROGNAME)
	@./$(PROGNAME)

install-deps :
	bower install

$(PROGNAME) : $(GO_SRCS)
	go build -o $(PROGNAME)

clean :
	rm -f $(PROGNAME)

.PHONY : all clean run install-deps
