conn = null;

function zip(arrays) {
  return arrays[0].map(function(_, i) {
    return arrays.map(function(array) {
      return array[i]
    })
  });
}

function getParameterByName(name, url) {
  if (!url) url = window.location.href;
  name = name.replace(/[\[\]]/g, "\\$&");
  var regex = new RegExp("[?&]" + name + "(=([^&#]*)|&|#|$)"),
    results = regex.exec(url);
  if (!results) return null;
  if (!results[2]) return '';
  return decodeURIComponent(results[2].replace(/\+/g, " "));
}

angular.module('maritimeDiceApp', ['leaflet-directive', 'ngMaterial', 'ngSanitize', 'ngMessages'])
  .config(function($logProvider) {
    $logProvider.debugEnabled(false);
  })
  .controller(
    'MainController', ['$window', '$scope', '$sce', '$timeout', '$interval', 'leafletBoundsHelpers', 'leafletData', '$mdSidenav', '$http',
      function($window, $scope, $sce, $timeout, $interval, leafletBoundsHelpers, leafletData, $mdSidenav, $http) {
        var root = this;

        // We begin by fixing some leaflet sizing issues
        var leafletInvalidate = function() {
          leafletData.getMap()
            .then(function(map) {
              map.invalidateSize();
            })

        }

        angular.element($window).bind('resize', function() {
          leafletInvalidate();
        });

        // Ugly, but onResize() is not enough
        $interval(leafletInvalidate, 500);

        $scope.toggleLeft = buildToggler('left');
        $scope.toggleRight = buildToggler('right');
        $scope.numPorts = 10;
        $scope.numCredit = 15;
        $scope.guestName = "Pupsi";
        $scope.toggleText = "Create";

        $timeout($scope.toggleRight);

        $scope.guestId = parseInt(getParameterByName("gameId"));
        $scope.connectGuest = function() {
          conn.send(JSON.stringify({
            Type: "join",
            GameId: $scope.guestId,
            Payload: $scope.guestName,
          }));
        }

        $scope.ownId = function() {
          return ($scope.guestId >= 0 ? $scope.guestName : "Admin");
        }

        $scope.getGameLinkTarget = function(game) {
          return ("http://" + window.location.host + "?gameId=" + game.Id);
        }

        $scope.fetch = function() {
          var ullat = $scope.map.bounds.northEast.lat;
          var ullon = $scope.map.bounds.northEast.lng;
          var lrlat = $scope.map.bounds.southWest.lat;
          var lrlon = $scope.map.bounds.southWest.lng;
          var url = "http://" + window.location.host + "/api/ports?ullat=" + ullat +
            "&ullon=" + ullon + "&lrlat=" + lrlat + "&lrlon=" + lrlon;

          $scope.code = null;
          $scope.response = null;

          $http({
            method: 'GET',
            url: url,
          }).
          then(function(response) {
            $scope.ports = response.data.map(function(singlePort) {
              return ({
                name: singlePort.Name,
                lat: singlePort.Location.Lat,
                lng: singlePort.Location.Lon,
                message: singlePort.Name
              });
            })
            console.log($scope.ports);
          }, function(response) {
            $scope.data = response.data || 'Request failed';
            $scope.status = response.status;
          });
        };

        function buildToggler(componentId) {
          return function() {
            $mdSidenav(componentId).toggle();
          }
        }

        $scope.start = function() {
          if ($scope.currentGame && $scope.currentGame.State == "created") {
            gameData = {
              "Type": "start",
              "GameId": $scope.currentGame.Id,
            }
            conn.send(JSON.stringify(gameData));
          } else {
            var fenceUpperLeft = {
              "Lat": $scope.map.bounds.northEast.lat,
              "Lon": $scope.map.bounds.northEast.lng,
            }

            var fenceLowerRight = {
              "Lat": $scope.map.bounds.southWest.lat,
              "Lon": $scope.map.bounds.southWest.lng,
            }

            gameData = {
              "Type": "create",
              "Payload": JSON.stringify({
                "NumPorts": $scope.numPorts,
                "NumCredit": $scope.numCredit,
                "FenceUpperLeft": fenceUpperLeft,
                "FenceLowerRight": fenceLowerRight,
              })
            }

            $scope.toggleText = "Start";

            conn.send(JSON.stringify(gameData));

          }
        }

        // React to clicks on markers by setting a new selected
        // dice stack or by moving the selected stack to
        // another location.
        $scope.$on("leafletDirectiveMarker.click",
          function(event, args) {
            console.log("Marker click");
            // Was a port selected?
            if (args.model.port) {
              var port = args.model.port;

              // Is a port currently selected?
              if ($scope.selectedStackId >= 0) {
                // Yes, move the stack
                console.log("Moving stack " + $scope.selectedStackId + " to " + port.Name);
                conn.send(JSON.stringify({
                  Type: "move",
                  GameId: $scope.currentGame.Id,
                  Payload: JSON.stringify({
                    DiceStackId: $scope.selectedStackId,
                    TargetPortId: port.Name,
                  })
                }))

                $scope.selectedStackId = undefined;

              } else {
                // No, select a new Stack
                var stack = Object.values($scope.currentGame.DiceStacks)
                  .find(function(stack) {
                    return (stack.CurrentPortId == port.Name);
                  });
                console.log("Clicked new port " + port.Name + " as " + $scope.ownId());

                $scope.selectedStackId = stack.DiceStackId;


              }
            }
          });


        // Store the currently selected stack so it can be
        // moved to another port.
        $scope.selectedStack = function() {
          var toReturn = Object.values($scope.currentGame.DiceStacks)
            .find(function(stack) {
              return (stack.DiceStackId == $scope.selectedStackId);
            });
          console.log("selectedStack() => " + JSON.stringify(toReturn));

          return (toReturn);
        }

        angular.extend($scope, {
          ports: [],
          defaults: {
            scrollWheelZoom: false
          },
          messages: [],
          connected: false,
          retryCount: 0,
          map: {
            bounds: leafletBoundsHelpers.createBoundsFromArray([
              [51.5093968565742, -0.0853747129440308],
              [40.5080880516331, 10.0904011726379395]
            ]),
            center: {}
          }
        });

        var logMessage = function(msg) {
          var trusted = $sce.trustAsHtml(msg);
          $scope.messages.push(trusted);
        }


        // Used to periodically pull out cached entries
        var refreshPromise = null;

        var handlers = {
          "all": function(data) {
            $scope.currentGame = data;

            var paths = $scope.currentGame.Routes.map(function(singlePath) {
              return ({
                  id: singlePath.Start + "_" + singlePath.End,
                  color: '#00800',
                  weight: 8,
                  latlngs: singlePath.Waypoints.map(function(waypoint) {
                    return ({
                      lat: waypoint.Lat,
                      lng: waypoint.Lon,
                    })
                  })
                })
              });

            pathObj = {}
            paths.forEach(function(p) {
              pathObj[p.id] = p
            })

            $scope.paths = pathObj;

            console.log('paths', $scope.paths);

            var renderPort = function(singlePort, matchingStacks) {
              var participant = $scope.currentGame.Participants.find(function(candidate) {
                return candidate.Name === singlePort.OwnerId;
              });

              if (participant == undefined) {
                participant = {
                  Color: "white"
                };
              }

              var stacks = matchingStacks.map(function(singleStack) {
                console.log(participant);
                console.log(participant.Color);
                return ("<li class='listPort'>" + singleStack.OwnerId + ":" + singleStack.Count + "</li>");
              }).join('');

              var div_icon = {
                type: 'div',
                html: '<div style="background-color:' + participant.Color + ';"><h4>' + singlePort.Name + '</h4><ul>' + stacks + '</ul></div>',
                iconSize: [125, 30 + 2.5 * stacks.length],
                port: singlePort,
              };
              return div_icon;
            }

            $scope.ports = data.Ports.map(function(singlePort) {
              var matchingStacks = Object.values(data.DiceStacks)
                .filter(function(singleStack) {
                  return singleStack.CurrentPortId === singlePort.Name;
                })

              console.log("Rendered port " + singlePort.Name + " with " + matchingStacks.length + " stacks");

              return ({
                name: singlePort.Name,
                size: singlePort.Size,
                lat: singlePort.Location.Lat,
                lng: singlePort.Location.Lon,
                icon: renderPort(singlePort, matchingStacks),
                port: singlePort,
                stacks: matchingStacks.splice(),
                stacksNum: matchingStacks.splice().length,
              });
            });

            console.log($scope.ports);
            console.log("received all data");
          },

          "round ended": function(data) {},
        }

        // Making sure websockets are supported
        if (window["WebSocket"]) {
          var connectPromise = null;

          var onMessage = function(evt) {
            $timeout(function() {
              var jsonData = JSON.parse(evt.data);
              var payload = JSON.parse(jsonData.Payload);

              console.log(payload);

              // If there is a handler, call that handler.
              if (handlers[jsonData.Type]) {
                handlers[jsonData.Type].call(root, payload);

              }
            });
          };

          var onClose = function(evt) {
            $scope.$apply(function() {
              $scope.connected = false;
              if (connectPromise == null) {
                logMessage('Connection lost, reconnecting ...');
                connect();
              }

            });
          };

          var onOpen = function(evt) {
            $scope.$apply(function() {
              // Cancel all possibly remaining attempts to reconnect
              $timeout.cancel(connectPromise);
              connectPromise = null;

              // And reset connection state
              $scope.retryCount = 0;
              $scope.connected = true;

              // And listen for errors
              conn.onclose = onClose;

              logMessage("Connection established");
            });
          };

          var connect = function() {
            if (!$scope.connected && connectPromise == null) {
              var wsUrl = "ws://" + window.location.host + "/ws";
              logMessage("Connecting to <code>" + wsUrl + "</code>");

              conn = new WebSocket(wsUrl);
              conn.onopen = onOpen;
              conn.onmessage = onMessage;

              // Possibly schedule the next timeout
              if ($scope.retryCount < 3) {
                connectPromise = $timeout(connect, 3000);
                $scope.retryCount++;
              }
            }
          };

          connect();

          $scope.manualReconnect = function() {
            logMessage("Attempting a manual reconnect");
            connect();
          }

          // Correctly close the websocket connection
          window.onbeforeunload = function() {
            console.log("Unloading and closing websocket");
            conn.onclose = function() {}; // disable onclose handler first
            conn.close()
          };


        } else {
          alert('Your browser does not support WebSockets.');
        }
      }
    ]
  );
