package main

import (
	"encoding/json"
	"github.com/gorilla/websocket"
	"log"
)

// A single connection that wants broadcasted messages.
type connection struct {
	// The websocket connection.
	ws *websocket.Conn

	// Buffered channel of outbound messages.
	send chan string

	// The hub.
	p *pool
}

// Receives messages for connections
func (c *connection) reader() {
	for {
		_, message, err := c.ws.ReadMessage()
		if err != nil {
			break
		}

		log.Printf("RCV %s: %s", c.ws.RemoteAddr(), message)

		var request GameMessage
		err = json.Unmarshal(message, &request)
		if err != nil {
			log.Fatal("Could not unmarshal GameMessage!", err)
		}

		switch request.Type {
		case "create":
			var createMsg CreateMessage
			err = json.Unmarshal([]byte(request.Payload), &createMsg)

			if err != nil {
				log.Fatal("Could not unmarshal CreateMessage!", err)
			}

			// createStr, _ := json.Marshal(createMsg)
			// log.Printf("RCV CREATE payload object: %s", createStr)
			// log.Printf("RCV CREATE payload string: %s", request.Payload)

			mkGame(createMsg.NumPorts, createMsg.FenceUpperLeft, createMsg.FenceLowerRight, "Admin", c)
			break
		default:
			g := getGame(request.GameId)
			log.Printf("Routing message to game %d", g.Id)
			g.MessageIn <- InternalMessage{Game: request, Sender: c}
			break
		}
	}

	c.ws.Close()
	c.p.unregister <- c
}

// Buffered send for messages
func (c *connection) writer() {
	for message := range c.send {
		b := []byte(message)
		err := c.ws.WriteMessage(websocket.TextMessage, b)
		if err != nil {
			break
		}

		// log.Printf("SENT %s: %s", c.ws.RemoteAddr(), message)
	}

	c.ws.Close()
	c.p.unregister <- c
}

// Groups together connections and allows broadcasting
type pool struct {
	// Registered connections, the exact value of the bool
	// is not actually used, we are more or less only interested
	// in the set semantics of the key of the map.
	connections map[*connection]bool

	// Messages to this channel are broadcasted among all registered
	// connections.
	broadcast chan string

	// Register requests from the connections.
	register chan *connection

	// Unregister requests from connections.
	unregister chan *connection
}

// Constructor function for an empty connection pool
func newPool() *pool {
	return &pool{
		broadcast:   make(chan string),
		register:    make(chan *connection),
		unregister:  make(chan *connection),
		connections: make(map[*connection]bool),
	}
}

func (p *pool) run() {
	for {
		select {

		case c := <-p.register:
			p.connections[c] = true
			log.Printf("New connection, now at %d", len(p.connections))
		case c := <-p.unregister:
			if _, ok := p.connections[c]; ok {
				delete(p.connections, c)
				close(c.send)

				OnDroppedConnection(c)
				log.Printf("Closed connection, now at %d", len(p.connections))
			}
		case m := <-p.broadcast:
			log.Printf("Broadcast: %s", m)
			for c := range p.connections {
				c.send <- m
			}
		}
	}
}
