package main

import (
	"bytes"
	"encoding/json"
	"fmt"
	"github.com/gorilla/websocket"
	"io/ioutil"
	"log"
	"math"
	"math/rand"
	"net/http"
	"strconv"
	"time"
)

type GeoPoint struct {
	Lat, Lon float64
}

func (lhs GeoPoint) Distance(rhs GeoPoint) float64 {
	return math.Sqrt(math.Pow(rhs.Lat-rhs.Lat, 2) + math.Pow(rhs.Lon+rhs.Lon, 2))
}

func (lhs GeoPoint) Add(rhs GeoPoint) GeoPoint {
	return lhs
}

type rawPort struct {
	Vname string  `json:"vname"`
	Size  string  `json:"port_size"`
	Lat   float64 `json:"lat"`
	Lon   float64 `json:"lon"`
}

type jsonResponse struct {
	Ports []rawPort `json:"ports"`
	Size  int       `json:"size"`
}

type Port struct {
	Name             string
	Size             string
	Location         GeoPoint
	OwnerId          string `json:",omitempty"`
	NeighbourPortIds []string
}

// Creates a new port from foreign port data
func mkPort(raw rawPort) *Port {
	return (&Port{
		Name:             raw.Vname,
		Size:             raw.Size,
		Location:         GeoPoint{Lat: raw.Lat, Lon: raw.Lon},
		NeighbourPortIds: make([]string, 0),
	})
}

// Grows stacks at a port
func (p *Port) tickGrowth(g *Game) {
	grownStacks := 0

	// Test whether this port should add dice to any dicestacks
	for _, s := range g.DiceStacks {
		// Only do this for stacks owned by the port
		if s.OwnerId == p.OwnerId && s.CurrentPortId == p.Name {
			// log.Printf("Stack %d (%s) at (%s) grew", s.DiceStackId, s.OwnerId, p.Name)
			s.Count++
			grownStacks++
		}
	}

	// Merging stacks at this port
	for _, participant := range g.Participants {
		var firstHere *DiceStack = nil

		// Find all dice stacks at thi port that belong to the current participant
		for _, s := range g.DiceStacks {
			if s.OwnerId == participant.Name && s.CurrentPortId == p.Name {
				if firstHere == nil {
					// The first stack gets all dice
					firstHere = s
					//log.Printf("First stack %d at port %s for %s", firstHere.DiceStackId, p.Name, participant.Name)
				} else {
					// All other stacks are removed
					firstHere.Count += s.Count
					delete(g.DiceStacks, s.DiceStackId)
					log.Printf("Merged stack %d into %d", s.DiceStackId, firstHere.DiceStackId)
				}
			}
		}
	}

	// log.Printf("Grew %d stacks at port %s, owned by %s", grownStacks, p.Name, p.OwnerId)
}

// Fights at the current port
func (p *Port) tickFight(g *Game) {
	// Grab all stacks that are at this port
	stacks := make([]*DiceStack, 0)
	// Sums for all participants
	throwSums := make(map[string]int32)
	for _, s := range g.DiceStacks {
		if s.CurrentPortId == p.Name {
			stacks = append(stacks, s)
			// Initialising sums for participants with 0
			// (Yes, this will overwrite several 0s,  but it works)
			throwSums[s.OwnerId] = 0
		}
	}

	// Is there a battle at this port?
	if len(throwSums) >= 2 {
		// Actual dice rolling
		for _, s := range stacks {
			for i := 0; i < s.Count; i++ {
				throwSums[s.OwnerId] = throwSums[s.OwnerId] + rand.Int31n(6) + 1
			}
		}

		// Compare dice rolls
		winner := p.OwnerId

		for k, v := range throwSums {
			if v > throwSums[winner] {
				winner = k
			}
		}

		log.Printf("Furious battle at %s, %s won with %d", p.Name, winner, throwSums[winner])
		log.Printf("Results where %s", throwSums)
		p.OwnerId = winner

		// Remove all dice stacks from other players at this port
		for _, s := range g.DiceStacks {
			if s.CurrentPortId == p.Name && s.OwnerId != winner {
				delete(g.DiceStacks, s.DiceStackId)
			}
		}
	}
}

// A route between two ports
type Route struct {
	Start, End string
	Distance   float64
	Waypoints  []GeoPoint
}

func mkRoute(start, end *Port) *Route {
	waypoints := make([]GeoPoint, 2)
	waypoints[0] = start.Location
	waypoints[1] = end.Location

	return &Route{
		Start:     start.Name,
		End:       end.Name,
		Distance:  start.Location.Distance(end.Location),
		Waypoints: waypoints,
	}
}

type GameMessage struct {
	Type    string
	GameId  int
	Payload string
}

// Sent by the client to create a new game
type CreateMessage struct {
	NumPorts        int      `json:"NumPorts"`
	NumCredit       int      `json:"NumCredit"`
	FenceUpperLeft  GeoPoint `json:"FenceUpperLeft"`
	FenceLowerRight GeoPoint `json:"FenceLowerRight"`
}

// Sent by the client to move a dice stack
type MoveMessage struct {
	DiceStackId  int
	TargetPortId string
}

type InternalMessage struct {
	Game   GameMessage
	Sender *connection
}

type Participant struct {
	Name       string
	Color      string
	Connection *connection `json:"-"`
}

func mkParticipant(name string, conn *connection) *Participant {
	return &Participant{
		Name:       name,
		Color:      "white",
		Connection: conn,
	}
}

type DiceVoyage struct {
	TargetPortId   string
	FromPortId     string
	CurrentSegment int
}

type DiceStack struct {
	DiceStackId   int
	Location      GeoPoint
	OwnerId       string `json:",omitempty"`
	Count         int
	CurrentPortId string `json:",omitempty"`
	Speed         float64

	// The following properties are used during a voyage
	Voyage *DiceVoyage
}

func mkDiceStack(game *Game, port *Port, owner *Participant, count int) *DiceStack {
	game.DiceIdCounter++
	toReturn := &DiceStack{
		DiceStackId:   game.DiceIdCounter,
		Location:      port.Location,
		CurrentPortId: port.Name,
		OwnerId:       owner.Name,
		Count:         count,
		Speed:         10,
	}

	game.DiceStacks[toReturn.DiceStackId] = toReturn

	return (toReturn)
}

func (d *DiceStack) tick() {
	// TODO: Currently beaming the ship to the target port
	if d.Voyage != nil {
		log.Printf("Stack #%d of %s moves from port %s to %s", d.DiceStackId, d.OwnerId, d.CurrentPortId, d.Voyage.TargetPortId)

		d.CurrentPortId = d.Voyage.TargetPortId
		d.Voyage = nil
	}
}

// Moves this dice stack to a new target
func (d *DiceStack) MoveTo(targetPortId string, game *Game) {
	// Is there no voyage happening and the target a new port?
	if d.Voyage == nil && (targetPortId != d.CurrentPortId) {
		d.Voyage = &DiceVoyage{
			TargetPortId:   targetPortId,
			FromPortId:     d.CurrentPortId,
			CurrentSegment: 0,
		}

		// Remove a single dice from the moving stack
		d.Count -= 1

		port := game.GetPort(d.CurrentPortId)
		participant := game.GetParticipant(d.OwnerId)
		// Create a new stack to stay behind
		mkDiceStack(game, port, participant, 1)
	}
}

type Game struct {
	Id           int
	State        string
	Ports        []*Port
	Participants []*Participant
	DiceStacks   map[int]*DiceStack
	Routes       []*Route

	MessageIn chan InternalMessage `json:"-"`
	Tick      chan int             `json:"-"`

	DiceIdCounter int `json:"-"`
}

func (g *Game) GetPort(portId string) *Port {
	for _, port := range g.Ports {
		if port.Name == portId {
			return port
		}
	}

	return nil
}

func (g *Game) GetParticipant(participantId string) *Participant {
	for _, participant := range g.Participants {
		if participant.Name == participantId {
			return participant
		}
	}

	return nil
}

// All available games
var games map[int]*Game = make(map[int]*Game)
var gameIdCounter int = 0

// Creates a new, empty gamestate
func mkGame(numPorts int, upperLeft, lowerRight GeoPoint, creatorName string, creatorConnection *connection) *Game {

	gameIdCounter++

	// Take relevant ports
	allPorts := GetPortData(upperLeft, lowerRight)
	relevantPorts := make([]*Port, numPorts)
	for i := 0; i < numPorts; i++ {
		relevantPorts[i] = allPorts[i]
	}

	routes := make([]*Route, 0)

	// Build routes
	for _, portInner := range relevantPorts {
		for _, portOuter := range relevantPorts {
			if portInner != portOuter {
				routes = append(routes, mkRoute(portInner, portOuter))
			}
		}
	}

	participants := make([]*Participant, 1)
	participants[0] = mkParticipant(creatorName, creatorConnection)

	toReturn := &Game{
		Id:           gameIdCounter,
		State:        "created",
		Ports:        relevantPorts,
		Participants: participants,
		DiceStacks:   make(map[int]*DiceStack),
		Routes:       routes,

		MessageIn:     make(chan InternalMessage, 10),
		Tick:          make(chan int, 1),
		DiceIdCounter: 0,
	}

	games[gameIdCounter] = toReturn

	go gameLoop(toReturn, gameIdCounter)

	log.Printf("Created new game %d with %d ports, now %d games", gameIdCounter, numPorts, len(games))

	return toReturn
}

func (g *Game) IsActive() bool {
	for _, p := range g.Participants {
		if p.Connection != nil {
			return (true)
		}
	}

	return (false)
}

// Retrieves a specific game
func getGame(gameId int) *Game {
	return games[gameId]
}

// Called when a player disconnects for whatever reason. Iterates over
// all games and erases that connection.
func OnDroppedConnection(c *connection) {
	for _, game := range games {
		for _, participant := range game.Participants {
			if participant.Connection == c {
				log.Printf("Dropped %s from game %d", participant.Name, game.Id)

				participant.Connection = nil
			}
		}
	}
}

// Game loop for a single game, reacts to all messages except for the
// game-creation message.
func gameLoop(game *Game, gameId int) {
	log.Printf("Started game-loop for game %d", gameId)

	// Broadcasts the current state as a whole
	// to all clients
	broadcastGameState := func() {
		log.Printf("Game id %d: Broadcasting gamestate", gameId)

		jsonGameState, err := json.Marshal(game)

		if err != nil {
			log.Fatal(err)
		}

		jsonState, err := json.Marshal(&GameMessage{
			GameId:  gameId,
			Type:    "all",
			Payload: string(jsonGameState),
		})

		if err != nil {
			log.Fatal(err)
		}

		// Broadcast the new state to all players
		for _, participant := range game.Participants {
			// log.Printf("Sent message to participant %d: %s", i, jsonState)
			if participant.Connection != nil {
				participant.Connection.ws.WriteMessage(websocket.TextMessage, jsonState)
			}
		}
	}

	broadcastGameState()

	for {
		select {
		case tick := <-game.Tick:
			log.Printf("Local tick #%d for game %d", tick, gameId)

			if game.State == "simulating" {
				// Move each dice stack
				for _, diceStack := range game.DiceStacks {
					diceStack.tick()
				}

				// Simulate each port
				// 1) Growth
				// 2) Fight!
				for _, port := range game.Ports {
					port.tickGrowth(game)
				}

				for _, port := range game.Ports {
					port.tickFight(game)
				}
			}

			broadcastGameState()

		// A message by a user?
		case msg := <-game.MessageIn:
			log.Printf("User message #%s for game %d\n", msg.Game.Payload, gameId)

			payload := msg.Game.Payload
			sender := msg.Sender
			switch msg.Game.Type {
			case "join":
				game.Participants = append(game.Participants, mkParticipant(payload, sender))
			case "start":
				// Fill ports with dice
				for i, port := range game.Ports {
					ownerIndex := i % len(game.Participants)
					participant := game.Participants[ownerIndex]
					stack := mkDiceStack(game, port, participant, 5)
					port.OwnerId = participant.Name
					log.Printf("Created initial dice stack #%d at %s for %s", stack.DiceStackId, port.Name, participant.Name)
				}

				// Assign Colours
				numParticipant := len(game.Participants)
				for i, participant := range game.Participants {
					hsvAngle := strconv.Itoa((100 / numParticipant) * i)
					participant.Color = "hsl(" + hsvAngle + ",100%,50%)"
				}

				// TODO: "started" state
				game.State = "simulating"
			case "move":
				var mvMsg MoveMessage
				err := json.Unmarshal([]byte(payload), &mvMsg)
				if err != nil {
					log.Fatal("Move message", err)
				}

				stack := game.DiceStacks[mvMsg.DiceStackId]
				stack.MoveTo(mvMsg.TargetPortId, game)
			}

			broadcastGameState()
		}
	}
}

// Runs the global game loop, which basically consists of a timer
// that regulary ticks all active games.
func GlobalGameLoop() {
	ticker := time.NewTicker(5 * time.Second)
	tickCount := 0

	for {
		select {
		case <-ticker.C:
			activeGames := 0
			tickCount++

			if len(games) > 0 {
				// Tick each game
				for _, game := range games {
					// In a non-blocking manner
					if game.IsActive() {
						activeGames++
						select {
						case game.Tick <- tickCount:
						default:
							log.Printf("No tick sent to #%d\n", game.Id)
						}
					}
				}

				if activeGames > 0 {
					log.Printf("Global tick %d for %d active games", tickCount, activeGames)
				}
			}
		}
	}
}

// Retrieve port data from an online service.
// TODO: This sometimes fails without a meaningful reason by
//       returning an empty port list.
func GetPortData(upperLeft, lowerRight GeoPoint) []*Port {
	readFromDisk := true
	var buf []byte

	// Should the port data be faked?
	if readFromDisk {
		log.Printf("Faking from disk")

		buf2, err := ioutil.ReadFile("./data/default_ports.json")
		if err != nil {
			log.Fatal(err)
		}

		buf = buf2

	} else {
		urlHost := "https://o6pqu2jl1a.execute-api.eu-central-1.amazonaws.com"
		urlApi := "/v1/ports_in_area"
		urlQuery := fmt.Sprintf("?lrlat=%f&lrlon=%f&ullat=%f&ullon=%f&zoomlevel=2",
			upperLeft.Lat, upperLeft.Lon, lowerRight.Lat, lowerRight.Lon)
		url := urlHost + urlApi + urlQuery

		log.Printf("Querying %s", url)

		res, err := http.Get(url)
		if err != nil {
			log.Fatal(err)
		}
		defer res.Body.Close()

		buf2 := new(bytes.Buffer)
		buf2.ReadFrom(res.Body)

		buf = buf2.Bytes()
	}

	var objmap jsonResponse
	err := json.Unmarshal(buf, &objmap)
	if err != nil {
		log.Fatal(err)
	}

	toReturn := make([]*Port, objmap.Size)

	for i, raw := range objmap.Ports {
		toReturn[i] = mkPort(raw)
	}

	fmt.Printf("Got %d ports\n", objmap.Size)
	return toReturn
}
